<?php
 
function register_cron_pay_recurrent() {
    if (! wp_next_scheduled ( 'pay_recurrent_niubiz' )) {
        wp_schedule_event(time(), 'daily', 'pay_recurrent_niubiz');
    }
}
 
if (! wp_next_scheduled ( 'pay_recurrent_niubiz' )) {
    wp_schedule_event(time(), 'daily', 'pay_recurrent_niubiz');
}
register_activation_hook(__FILE__, 'register_cron_pay_recurrent');
 
function pay_recurrent_niubiz() {
    addNIUBIZ_LOG(
        array(
            "type"=>"cron",
            "date"=> date("d-m-Y"),
        )
    );
    $args = array(
        'return' => 'ids',
        'meta_key' => 'niubizsuscription',
    );
    
    $users = get_users( $args );
    for ($i=0; $i < count($users); $i++) { 
        $user = $users[$i];
        $user_id = $user->ID;
        $product_id = get_user_meta(
            $user_id,
            "niubizsuscription",
            true
        );
        $fecha_user = get_user_meta(
            $user_id,
            "niubizpayDate",
            true
        );
        $fecha_actual = date("d-m-Y");
        $fecha_actual = strtotime($fecha_actual);
        $fecha_actual = intval($fecha_actual);
        $fecha_user = intval($fecha_user);
        if($fecha_user <= $fecha_actual){
            $result = Niubiz_payRecurrentUser($user_id);
            addNIUBIZ_LOG(
                array(
                    "type"=>"cronpay",
                    "result"=>$result,
                    "date"=> date("d-m-Y"),
                )
            );
            update_user_meta(
                $user_id,
                "niubizsuscriptionTYPE",
                "cron"
            );
            $fecha_actual = date("d-m-Y");
            update_user_meta(
                $user_id,
                "niubizpayDate",
                strtotime($fecha_actual."+ 1 month")
            );
        }
    }
}
add_action('pay_recurrent_niubiz', 'pay_recurrent_niubiz');