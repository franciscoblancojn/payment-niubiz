<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$email = $_GET["email"];

if(empty($email)){
    echo json_encode(array(
        "type"=>"error",
        "msj"=>"empty email"
    ));
    exit;
}

$user = get_user_by( 'email',$email );
if(!$user){
    echo json_encode(array(
        "type"=>"error",
        "msj"=>"invalid email"
    ));
    exit;
}
$user_id = $user->ID;

$date = get_user_meta($user_id,"niubizpayDate",true);
echo json_encode(array(
    "type"=>"ok",
    "date"=>$date
));
exit;