<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}
$signature = $_POST["signature"];
if( empty( $signature) ) {
    echo json_encode(array(
        "status" => 400,
        "data" => "signature empty"
    ));
    exit;
}
$order_id = $_POST["order_id"];
if( empty( $order_id) ) {
    echo json_encode(array(
        "status" => 400,
        "data" => "order_id empty"
    ));
    exit;
}

$niubizPayment = new WC_Niubiz_Gateway();
$api = $niubizPayment->api();

$result = $api->anulacion($signature);

$status = $result["status"];
if($status==200){
    update_post_meta(
        $order_id,
        "anulationPaymentNiubiz",
        json_encode($result)
    );
}

echo json_encode($result);