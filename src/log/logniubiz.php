<?php
if(NIUBIZ_LOG){
    function add_NIUBIZ_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'NIUBIZ_LOG',
                'title' => 'NIUBIZ_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=NIUBIZ_LOG'
            )
        );
    }

    function NIUBIZ_LOG_option_page()
    {
        add_options_page(
            'Log Aveonline',
            'Log Aveonline',
            'manage_options',
            'NIUBIZ_LOG',
            'NIUBIZ_LOG_page'
        );
    }

    function NIUBIZ_LOG_page()
    {
        if($_GET["user_id"]!= "" && $_GET["user_id"]!= null){
            $fecha_actual = date("d-m-Y");
            update_user_meta(
                $_GET["user_id"],
                "niubizpayDate",
                strtotime($fecha_actual)
            );
        }
        $log = get_option("NIUBIZ_LOG");
        if($log === false || $log == null || $log == ""){
            $log = "[]";
        }
        ?>
        <script>
            const log = <?=$log?>;
        </script>
        <h1>
            Solo se guardan las 100 peticiones
        </h1>
        <pre><?php var_dump(array_reverse(json_decode($log,true)));?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_NIUBIZ_LOG_option_page', 100);

    add_action('admin_menu', 'NIUBIZ_LOG_option_page');

}

function addNIUBIZ_LOG($newLog)
{
    if(!NIUBIZ_LOG){
        return;
    }
    $log = get_option("NIUBIZ_LOG");
    if($log === false || $log == null || $log == ""){
        $log = "[]";
    }
    $log = json_decode($log);
    $log[] = $newLog;
    $log = array_slice($log, -100,100); 
    update_option("NIUBIZ_LOG",json_encode($log));
}