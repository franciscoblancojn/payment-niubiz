<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


// do_action( 'woocommerce_email_header', $email_heading, $email );

?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/uFMfIWE.png"
        class="CToWUd a6T" tabindex="0">
    <p>
        Hola <strong><?=$user_login?></strong>,
    </p>
    <p>
        Se ha recibido tu solicitud para un restablecimiento de contraseña. 
    </p>
    <p>
        Si fue un error, simplemente ignora este correo electrónico y no pasará nada. 
    </p>
    <p>
        Para restablecer tu contraseña, deberás ingresar al siguiente link: 
        <a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'id' => $user_id ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>"><?php // phpcs:ignore ?>
            Aqui
        </a>
    </p>
    <?php
    /**
     * Show user-defined additional content - this is set in each email's settings.
     */
    if ( $additional_content ) {
        echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
    }

    // do_action( 'woocommerce_email_footer', $email );
    ?>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/mCoPI9a.png"
        class="CToWUd">
</div>
<?php