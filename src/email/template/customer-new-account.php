<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 6.0.0
 */

defined( 'ABSPATH' ) || exit;

// do_action( 'woocommerce_email_header', $email_heading, $email );
$user_login = $email->user_login;
$user_email = $email->user_email;
$password = $email->user_pass;
?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/5GZlvZ2.png"
        class="CToWUd a6T" tabindex="0">
    <div style="padding:15px">
        <p>
            Hola <?=$user_login ?>
        </p>
        <p>
            Acabas de adquirir tu membresía para nuestra plataforma de coaching nutricional online.
        </p>
        <p>
            Podrás hacer tu evaluación, acceder a tus resultados y a todo nuestro contenido, recomendaciones y guías nutricionales para el logro de tus objetivos.
        </p>
        <p>
            Accede a la plataforma con los siguientes accesos:
        </p>
        <p style="margin-bottom:4px">
            <strong>Login: <br/>  <?=$user_email ?> </strong>
        </p>
        <p style="margin:0">
            <strong>Contraseña: <br/> <?=$password?></strong>
        </p>
        <p>
        ¡Complementa tu guía nutricional con un entrenamiento Smart!
        </p>
    </div>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/tm2gDR9.png"
        class="CToWUd">
</div>
<?php

// do_action( 'woocommerce_email_footer', $email );