<?php

defined( 'ABSPATH' ) || exit;

?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="http://checkoutpe.smartfit.com.pe/wp-content/uploads/2022/04/WhatsApp-Image-2022-03-31-at-4.01.30-PM.jpeg"
        class="CToWUd a6T" tabindex="0">
    <div style="padding:15px">
        <p>
            Hola <strong><?=$user_name?></strong>
        </p>
        <p>
            Listo! Recibimos tu solicitud para migrar al plan <?=$plan?>, el cual se encontrará activo a partir del <?=date("d-m-Y")?>
        </p>
        <p>
            Recuerda que solo el Plan Smart Fit Nutri PRO te permitirá gozar de los siguientes beneficios adicionales:  
        </p>
        <ul>
            <li>
                Consulta de coaching nutricional online mensual con una Nutricionista.
            </li>
            <li>
                Gráficos evolutivos porcentuales más detallados.
            </li>
            <li>
                Análisis y recomendaciones de:
            </li>
            <li>
                Indicador relación cintura/cadera
            </li>
            <li>
                Macronutrientes de cada una de tus comidas y todos tus micronutrientes
            </li>
        </ul>
        <p>
            ¡Complementa tu guía nutricional con un entrenamiento Smart!
            <a href="http://www.smartfitnutri.com.pe/">http://www.smartfitnutri.com.pe/</a>
        </p>
    </div>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/61LMk8y.png"
        class="CToWUd">
</div>
<?php
