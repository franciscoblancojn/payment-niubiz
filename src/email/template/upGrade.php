<?php

defined( 'ABSPATH' ) || exit;

?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="http://checkoutpe.smartfit.com.pe/wp-content/uploads/2022/04/WhatsApp-Image-2022-04-07-at-3.12.08-PM.jpeg"
        class="CToWUd a6T" tabindex="0">
    <div style="padding:15px">
        <p>
            Hola <strong><?=$user_name?></strong>
        </p>
        <p>
            ¡Felicitaciones por el UpGrade! Desde <?=date("d-m-Y")?> podrás disfrutar de los beneficios de tu plan SMART NUTRI PRO los cuales son los siguientes:
        </p>
        <ul>
            <li>
                Consulta de coaching nutricional online mensual con una Nutricionista.
            </li>
            <li>
                Gráficos evolutivos porcentuales más detallados.
            </li>
            <li>
                Análisis y recomendaciones de:
            </li>
            <li>
                Indicador relación cintura/cadera
            </li>
            <li>
                Macronutrientes de cada una de tus comidas y todos tus micronutrientes
            </li>
        </ul>
        <p>
            Desde <?=date("d-m-Y")?> formarás parte de nuestros clientes con plan SMART NUTRI PRO. Te estaremos mandando comunicación exclusiva sobre novedades de la plataforma.
        </p>
        <p>
            ¡Complementa tu guía nutricional con un entrenamiento Smart!
            <a href="http://www.smartfitnutri.com.pe/">http://www.smartfitnutri.com.pe/</a>
        </p>
    </div>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/61LMk8y.png"
        class="CToWUd">
</div>
<?php
