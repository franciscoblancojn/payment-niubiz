<?php

defined( 'ABSPATH' ) || exit;

?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="http://checkoutpe.smartfit.com.pe/wp-content/uploads/2022/04/WhatsApp-Image-2022-04-07-at-3.21.30-PM.jpeg"
        class="CToWUd a6T" tabindex="0">
    <div style="padding:15px">
        <p>
            Hola <strong><?=$user_name?></strong>
        </p>
        <p>
            Hemos recibido tu pedido de suscripción al plan <?=$plan?>, y se está procesando.
        </p>
        <p>
            Recibirás un correo de confirmación luego de que este proceso haya culminado. 
        </p>
        <p>
            Tu suscripción al plan <?=$plan?> se encontrará vigente desde el <?=date("d-m-Y")?>. 
        </p>
        <p>
            ¡Complementa tu guía nutricional con un entrenamiento Smart!
            <a href="http://www.smartfitnutri.com.pe/">http://www.smartfitnutri.com.pe/</a>
        </p>
    </div>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/61LMk8y.png"
        class="CToWUd">
</div>
<?php
