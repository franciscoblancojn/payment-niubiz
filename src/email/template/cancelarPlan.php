<?php

defined( 'ABSPATH' ) || exit;

?>
<div style="max-width:900px;margin:0 auto">
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/PZWICtu.png"
        class="CToWUd a6T" tabindex="0">
    <p>
        Hola <strong><?=$user_name?></strong>
    </p>
    <p>
        Lamentamos que no puedas continuar con nosotros. De acuerdo a tu solicitud, tu
        suscripción queda cancelada desde el <?=date("d-m-Y")?>
    </p>
    <p>
        Estaríamos encantados de que volvieras con nosotros. Si cambias de opinión, solo
        tendrás que <strong><a href="http://www.smartfitnutri.com.pe">reactivar tu suscripción</a></strong> para disfrutar del mejor contenido sobre
        nutrición inteligente.
    </p>
    <img alt="welcome" style="width:100%;height:auto"
        src="https://i.imgur.com/61LMk8y.png"
        class="CToWUd">
</div>
<?php
