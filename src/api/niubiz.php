<?php

class apiNiubiz{
    private $DEVUSER = "integraciones@niubiz.com.pe";
    private $DEVPASSWORD = '_7z3@8fF';
    private $DEVMERCHANTID = "456879852";

    private $URLPRODUCTION = "https://apiprod.vnforapps.com";
    private $URLSANDBOX = "https://apisandbox.vnforappstest.com";

    function __construct($atts = array())
    {
        $this->MODE = $atts["MODE"] == "DEV" ? "DEV" : "PRO";
        $this->URLAPI = $this->MODE == "DEV" ? $this->URLSANDBOX : $this->URLPRODUCTION;
        $this->merchantId = $atts["merchantId"];
        $this->merchantIdRecurrent = $atts["merchantIdRecurrent"];

        $userName = $atts["userName"];
        $password = $atts["password"];

        if($this->MODE == "DEV"){
            $userName = $this->DEVUSER;
            $password = $this->DEVPASSWORD;
            $this->merchantId = $this->DEVMERCHANTID;
            $this->merchantIdRecurrent = $this->DEVMERCHANTID;
        }

        $this->token = $this->security($userName,$password);
    }
    /**
     * request to api
     * @author francisco blanco
     * @param json,url,autorization
     * @return response
     */
    private function request($url, $autorization , $method = "POST",$json= array())
    {
        $arrayRequest = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($json),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "accept: application/json",
                $autorization
            ),
        );
        $curl = curl_init();
        curl_setopt_array($curl, $arrayRequest);
        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if(($response[0] == "{" || $response[0] == "[") && ($response[strlen($response) -1] == "}" || $response[strlen($response) -1] == "]")){
            $response = json_decode($response,true);
        }
        $result = array(
            "status" => $httpcode,
            "data" => $response,
        );
        addNIUBIZ_LOG(array(
            "request" => array(
                "url" =>$url, 
                "autorization" => $autorization , 
                "method" => $method,
                "json" => $json
            ),
            "result"=>$result,
        ));
        return $result;
    }
    /**
     * security genera token for api
     * @author francisco blanco
     * @param userName,password
     * @return Token
     */
    private function security($userName,$password)
    {
        $autorization = "Authorization: Basic ".base64_encode($userName.":".$password);
        $result = $this->request(
            $this->URLAPI."/api.security/v1/security",
            $autorization,
            "GET",
        );
        $this->STATUSTOKEN = $result['status'];
        $this->TOKEN = $result['data'];
    }
    /**
     * authorization genera token for payment
     * @author francisco blanco
     * @param userName,password
     * @return Token
     */
    public function authorization($atts = array())
    {
        if($this->STATUSTOKEN != 201){
            return array(
                "status" => $this->STATUSTOKEN,
                "data" => $this->TOKEN,
            );
        }
        $merchantId = $atts["channel"] == "web" ? $this->merchantId : $this->merchantIdRecurrent;
        $autorization = "Authorization:".$this->TOKEN;
        $url = $this->URLAPI."/api.authorization/v3/authorization/ecommerce/".$merchantId;
        $array = array(
            "channel"=> $atts["channel"],//web,recurrent
            "captureType"=> "manual",
            "countable"=> true,
            "order" => $atts["order"],
            "card"=> $atts["card"],
            "cardHolder"=> $atts["cardHolder"]
        );
        // {
        //     "channel": "web",
        //     "captureType": "manual",
        //     "countable": true,
        //     "order": {
        //     "purchaseNumber": "348",
        //     "amount": "10.00",
        //     "currency": "PEN",
        //     },
        //     "card": {
        //     "tokenId": "7000010038742635",
        //     "registerFrequent": true,
        //     "useFrequent": false
        //     },
        //     "cardHolder": {
        //     "email": "integraciones.niubiz@necomplus.com"
        //     }
        //     }
        $result = $this->request(
            $url,
            $autorization,
            "POST",
            $array
        );
        return $result;
    }
    /**
     * anulacion payment
     * @author francisco blanco
     * @param signature
     * @return result
     */
    public function anulacion($signature)
    {
        if($this->STATUSTOKEN != 201){
            return array(
                "status" => $this->STATUSTOKEN,
                "data" => $this->TOKEN,
            );
        }
        $autorization = "Authorization:".$this->TOKEN;
        $url = $this->URLAPI."/api.authorization/v3/void/ecommerce/".$this->merchantId."/".$signature;
        $result = $this->request(
            $url,
            $autorization,
            "PUT",
        );
        return $result;
    }
    /**
     * sessionToken generate sessionToken for use in create form payment
     * @author francisco blanco
     * @param atts
     * @return token
     */
    private function sessionToken($atts = array())
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $url = $this->URLAPI . "/api.ecommerce/v2/ecommerce/token/session/".$this->merchantId;
        $autorization = "Authorization:".$this->TOKEN;
        $json = array(
            "channel"=> "paycard",
            "amount"=> $atts["amount"],
            "antifraud"=> array(
                "clientIp"=> $ip,
                "merchantDefineData"=> array(
                    "MDD4"  => $atts["cardholderemail"],
                    "MDD21" => "0",
                    "MDD32" => $atts["user_id"],
                    "MDD75" => "invitado",
                    "MDD77" => "1",
                )
            )
        );
        $result = $this->request(
            $url,
            $autorization,
            "POST",
            $json
        );
        $this->STATUSTOKENSESSION = $result['status'];
        $this->TOKENSESSION = $result['data'];
        return $result;
    }
    /**
     * printForm generate html for payment
     * @author francisco blanco
     * @param atts
     * @return html
     */
    public function printForm($atts = array())
    {
        $this->sessionToken($atts);

        if($this->STATUSTOKENSESSION !=200){
            ?>
                <h1>
                    Error
                </h1>
                <pre>
                    <?php var_dump($this->TOKENSESSION);?>
                </pre>
            <?php
            return ;
        }

        $urlForm = $this->MODE == "DEV" ? "https://static-content-qas.vnforapps.com/vTokenSandbox/js/checkout.js" : "https://static-content.vnforapps.com/vToken/js/checkout.js";
        ?>
        <script type="text/javascript" src="<?=$urlForm?>"></script>
        <script type="text/javascript">
            function checkout() {
                VisanetCheckout.configure({
                    action: '<?=$atts["action"]?>',
                    sessiontoken:'<?=$this->TOKENSESSION["sessionKey"]?>',
                    channel: 'paycard',
                    merchantid: '<?=$this->merchantId?>',
                    purchasenumber: '<?=$atts["order_id"]?>',
                    amount: "<?=$atts["amount"]?>",
                    cardholdername: '<?=$atts["cardholdername"]?>',
                    cardholderlastname: '<?=$atts["cardholdername"].$atts["cardholderlastname"]?>',
                    cardholderemail: '<?=$atts["cardholderemail"]?>',
                    usertoken: '<?=$atts["usertoken"]?>',
                    expirationminutes: '20',
                    timeouturl:'<?=$atts["timeouturl"]?>',
                    merchantlogo:'<?=$atts["merchantlogo"]?>',
                    formbuttoncolor: '<?=$atts["formbuttoncolor"]?>',
                    formbuttontext: '<?=$atts["formbuttontext"]?>',
                    showamount: 'TRUE',
                });
                VisanetCheckout.open();
            }
            checkout()
        </script>
        <?php
    }
    /**
     * generateTokenByPay generate pay by token
     * @author francisco blanco
     * @param token
     * @return result
     */
    public function generateTokenByPay($token)
    {
        if($this->STATUSTOKEN != 201){
            return array(
                "status" => $this->STATUSTOKEN,
                "data" => $this->TOKEN,
            );
        }
        $autorization = "Authorization:".$this->TOKEN;
        $url = $this->URLAPI."/api.ecommerce/v2/ecommerce/token/card/".$this->merchantId."/".$token;
        $result = $this->request(
            $url,
            $autorization,
            "GET",
        );
        return $result;
    }
}
