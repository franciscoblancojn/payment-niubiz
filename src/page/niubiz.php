<?php
function NIUBIZ_create_menu() {

	//create new top-level menu
	add_menu_page('NIUBIZ Settings', 'NIUBIZ', 'administrator', __FILE__, 'NIUBIZ_settings_page' , NIUBIZ_URL.'src/img/NIUBIZ.png' );

	//call register settings function
	add_action( 'admin_init', 'register_NIUBIZ_settings' );
}
add_action('admin_menu', 'NIUBIZ_create_menu');


function register_NIUBIZ_settings() {
	//register our settings
	register_setting( 'NIUBIZ-settings-group', 'new_option_name' );
	register_setting( 'NIUBIZ-settings-group', 'some_other_option' );
	register_setting( 'NIUBIZ-settings-group', 'option_etc' );
}

function NIUBIZ_settings_page() {
    $args = array(
        'return' => 'ids',
        'meta_key' => 'niubizsuscription',
    );
    
    $users = get_users( $args );
    $fecha_actual = date("d-m-Y");
    $fecha_actual = strtotime($fecha_actual);

    if($_POST["submit"] == "Save"){
        update_option("NIUBIZ_Token",$_POST["NIUBIZ_Token"]);
    }
    ?>
    <div class="wrap">
        <h1>
            Niubiz
        </h1>
        <span hidden><?=$fecha_actual?></span>

        <form method="post">
            <h3>
                Config
            </h3>
            <div>
                <label for="NIUBIZ_Token"><strong>Token</strong></label>
                <br>
                <input
                class="input-text regular-input "
                type="text" value="<?=get_option( 'NIUBIZ_Token' )?>" name="NIUBIZ_Token" id="NIUBIZ_Token" />
            </div>
            <hr>
            <input type="submit" id="submit" name="submit" class="button action" value="Save">
        </form>
        <?php
            if(count($users) != 0){
                ?>
                <a href="" id="aDonwloadCSV" hidden></a>
                <button class="button action" onclick="descargarCSV()">
                    Descargar CSV
                </button>
                <table class="wp-list-table widefat fixed striped table-view-list posts">
                    <thead>
                        <th>User id</th>
                        <th>User Email</th>
                        <th>Suscription</th>
                        <th>Proximo Pago</th>
                    </thead>
                    <tbody>
                        <?php
                            $users_json = [];
                            for ($i=0; $i < count($users); $i++) { 
                                $user = $users[$i];
                                $user_id = $user->ID;
                                $suscription = get_user_meta(
                                    $user_id,
                                    "niubizsuscription",
                                    true
                                );
  
                                $productSuscription = wc_get_product( $suscription );

                                $payDate = get_user_meta(
                                    $user_id,
                                    "niubizpayDate",
                                    true
                                );
                                $users_json[] = array(
                                    "user_id" => $user_id,
                                    "user_email" => $user->data->user_email,
                                    "suscription_id" => $suscription,
                                    "suscription_name" => $productSuscription->get_name(),
                                    "date" => date("d-m-Y",$payDate),
                                );
                                ?>
                                <tr>
                                    <td><?=$user_id?></td>
                                    <td><?=$user->data->user_email?></td>
                                    <td><?=$productSuscription->get_name()?></td>
                                    <td><?=date("d-m-Y",$payDate)?><span hidden><?=$payDate?></span></td>
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
                <script>
                    const aDonwloadCSV = document.getElementById("aDonwloadCSV")
                    const users_json = <?=json_encode($users_json)?>;
                    function ConvertToCSV(objArray) {
                        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
                        console.log(array[0]);
                        var str = '';

                        for (var i = 0; i < array.length; i++) {
                            var line = '';
                            for (var index in array[i]) {
                                if (line != '') line += ','

                                line += array[i][index];
                            }

                            str += line + '\r\n';
                        }

                        return str;
                    }
                    const descargarCSV = () => {
                        console.log(users_json);
                        const CSV = ConvertToCSV([
                            Object.keys(users_json[0]),
                            ...users_json
                        ]);
                        const uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
                        aDonwloadCSV.href = uri
                        aDonwloadCSV.download = (new Date()).toDateString() + ".csv";
                        aDonwloadCSV.click()
                    }
                </script>
                <?php
            }else{
                ?>
                <h2>
                    No hay suscripciones
                </h2>
                <?php
            }
        ?>
    </div>
    <script>
        const url = "<?=NIUBIZ_URL?>src/routes/anulationSuscription.php";
        const anulacionRecurent = async (signature,order_id) => {
            try {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                var raw = JSON.stringify({
                    signature,
                    order_id
                });
                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };
                const response = await fetch(url, requestOptions)
                const result = await response.json()
                console.log(result);
                if(result.status != 200){
                    alert(result.data)
                }else{
                    window.location.reload()
                }
            } catch (error) {
                console.log(error);
                alert(error)
            }
        }
    </script>
    <?php 
}