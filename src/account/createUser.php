<?php
function niubiz_createUser($email,$name)
{
    if ( false == email_exists( $email ) ) {
        $random_password_temporal = wp_generate_password( $length = 12, $include_standard_special_chars = false );
        // $user_id = wp_create_user( $email, $random_password_temporal, $email );
        $user_id = wc_create_new_customer( $email, $name , $random_password_temporal);
        return $user_id;
    } else {
        $user = get_user_by( 'email',$email );
        $user_id = $user->ID;
        return $user_id;
    }
}