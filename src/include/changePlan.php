<?php

function NIUBIZ_get_changePlanReport()
{
    $NIUBIZ_changePlanReport = get_option("NIUBIZ_changePlanReport");
    if($NIUBIZ_changePlanReport === false || $NIUBIZ_changePlanReport == null || $NIUBIZ_changePlanReport == ""){
        $NIUBIZ_changePlanReport = "[]";
    }
    $NIUBIZ_changePlanReport = json_decode($NIUBIZ_changePlanReport,true);
    return $NIUBIZ_changePlanReport;
}

function NIUBIZ_addchangePlan($user_id,$oldPlan,$newPlan)
{
    $newNIUBIZ_changePlanReport = array(
        "date" => date("m/d/Y"),
        "user_id"=>$user_id,
        "oldPlan"=>$oldPlan,
        "newPlan"=>$newPlan,
    );
    $NIUBIZ_changePlanReport = NIUBIZ_get_changePlanReport();
    $NIUBIZ_changePlanReport[] = $newNIUBIZ_changePlanReport;
    update_option("NIUBIZ_changePlanReport",json_encode($NIUBIZ_changePlanReport));
}