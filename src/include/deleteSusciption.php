<?php

function NIUBIZ_get_deleteSusciptionReport()
{
    $NIUBIZ_deleteSusciptionReport = get_option("NIUBIZ_deleteSusciptionReport");
    if($NIUBIZ_deleteSusciptionReport === false || $NIUBIZ_deleteSusciptionReport == null || $NIUBIZ_deleteSusciptionReport == ""){
        $NIUBIZ_deleteSusciptionReport = "[]";
    }
    $NIUBIZ_deleteSusciptionReport = json_decode($NIUBIZ_deleteSusciptionReport,true);
    return $NIUBIZ_deleteSusciptionReport;
}

function NIUBIZ_adddeleteSusciption($user_id,$plan)
{
    $newNIUBIZ_deleteSusciptionReport = array(
        "date" => date("m/d/Y"),
        "dateFinal" => date("m/d/Y"),
        "user_id"=>$user_id,
        "plan"=>$plan,
    );
    $NIUBIZ_deleteSusciptionReport = NIUBIZ_get_deleteSusciptionReport();
    $NIUBIZ_deleteSusciptionReport[] = $newNIUBIZ_deleteSusciptionReport;
    update_option("NIUBIZ_deleteSusciptionReport",json_encode($NIUBIZ_deleteSusciptionReport));
}