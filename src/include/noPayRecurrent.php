<?php

function NIUBIZ_get_noPayRecurrentSusciptionReport()
{
    $NIUBIZ_noPayRecurrentSusciptionReport = get_option("NIUBIZ_noPayRecurrentSusciptionReport");
    if($NIUBIZ_noPayRecurrentSusciptionReport === false || $NIUBIZ_noPayRecurrentSusciptionReport == null || $NIUBIZ_noPayRecurrentSusciptionReport == ""){
        $NIUBIZ_noPayRecurrentSusciptionReport = "[]";
    }
    $NIUBIZ_noPayRecurrentSusciptionReport = json_decode($NIUBIZ_noPayRecurrentSusciptionReport,true);
    return $NIUBIZ_noPayRecurrentSusciptionReport;
}

function NIUBIZ_addnoPayRecurrentSusciption($user_id,$plan)
{
    $newNIUBIZ_noPayRecurrentSusciptionReport = array(
        "date" => date("m/d/Y"),
        "dateFinal" => date("m/d/Y",get_user_meta($user_id,"niubizpayDate",true)),
        "user_id"=>$user_id,
        "plan"=>$plan,
    );
    $NIUBIZ_noPayRecurrentSusciptionReport = NIUBIZ_get_noPayRecurrentSusciptionReport();
    $NIUBIZ_noPayRecurrentSusciptionReport[] = $newNIUBIZ_noPayRecurrentSusciptionReport;
    update_option("NIUBIZ_noPayRecurrentSusciptionReport",json_encode($NIUBIZ_noPayRecurrentSusciptionReport));
}