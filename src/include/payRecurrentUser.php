<?php

function MISP_isSmargit($login,$auth,$authToken,$urlApi) {
    //new request login
    $curl = curl_init();
  
    curl_setopt_array($curl, array(
    CURLOPT_URL => $urlApi,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array(
        'login' => $login,
        'authentication_field' => 'client_token',
        'authentication_value' => $auth
    ),
    CURLOPT_HTTPHEADER => array(
        "authorization: ".$authToken
    ),
    ));
  
    $response = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
    curl_close($curl);
  
    $response = json_decode($response);
  
    if($response->errors == "invalid_login"){
        return array(
            "type"  =>  "Error",
            "error" =>  "invalid_login"
        );
    }else{
        $token = $response->auth_token;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $urlApi."/".$token,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: ".$authToken
        ),
        ));
  
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
  
        curl_close($curl);
  
        $response = json_decode($response);
        if($response->errors == "invalid_single_access_token"){
            return array(
                "type"  => "Error",
                "error" => "invalid_single_access_token"
            );
        }else{
            if($response->status == "active" ){
                return array(
                    'type' => "ok", 
                    'payload' => array(
                        'auth_token' => $token, 
                    ),
                );
            }else{
                return array(
                    "type"  => "Error",
                    "error" => "status no active",
                );
            }
        }
    }
    return;
}
function Niubiz_payRecurrentUser($user_id)
{
    global $woocommerce;
    $user = get_user_by('id', $user_id);
    if(!$user){
        return "Error user no existe";
    }
    $niubizPayment = new WC_Niubiz_Gateway();
    $api = $niubizPayment->api();

    $token = get_user_meta(
        $user_id,
        "niubiztoken",
        true
    );
    $suscription = get_user_meta(
        $user_id,
        "niubizsuscription",
        true
    );
    $customer = new WC_Customer( $user_id );
    $address = array(
        'first_name' => $customer->get_billing_first_name(),
        'last_name'  => $customer->get_billing_last_name(),
        'company'    => $customer->get_billing_company(),
        'email'      => $customer->get_email(),
        'phone'      => get_user_meta( $user_id, 'billing_phone', true ),
        'address_1'  => $customer->get_billing_address_1(),
        'address_2'  => $customer->get_billing_address_2(),
        'city'       => $customer->get_billing_city(),
        'state'      => $customer->get_billing_state(),
        'postcode'   => $customer->get_billing_postcode(),
        'country'    => $customer->get_billing_country()
    );
    $billing_cedula = get_user_meta($user_id,"billing_cedula",true);
    $order = wc_create_order();
    $order->add_product( get_product($suscription), 1);
    $order->set_address( $address, 'billing' );
    $r = MISP_isSmargit(
        $billing_cedula,
        $niubizPayment->get_option( 'authSmarfit' ),
        $niubizPayment->get_option( 'authTokenSmarfit' ),
        $niubizPayment->get_option( 'urlApiSmarfit' )
    ); 
    if($r["type"] == "ok"){
        //addDiscounte
        $coupon = explode(",",$niubizPayment->get_option( 'couponBySmarfit' ));
        for ($i=0; $i < count($coupon); $i++) { 
            $order->apply_coupon($coupon[$i]);
        }
    }   
    $order->calculate_totals();
    $order_id = $order->get_id();



    $config = array(
        "channel" => "recurrent",
        "order" => array(
            "purchaseNumber"=> $order_id,
            "amount"=> wc_format_decimal($order->get_total(),2),
            "currency"=> $niubizPayment->get_option( 'currency' ),
        ),
        "card" => array(
            "tokenId" =>  $token,
        ),
        "cardHolder"=> array(
            "email"=>$order->get_billing_email(),
            'firstName' => $customer->get_billing_first_name(),
            'lastName'  => $customer->get_billing_last_name(),
        )
    );
    
    $result = $api->authorization($config);
    if(200 == $result["status"]){
        update_post_meta(
            $order_id,
            "niubiztransaction",
            json_encode($result["data"])
        );
        update_post_meta(
            $order_id,
            "payRecurrent",
            "yes"
        );
        $fecha_actual = date("d-m-Y");
        update_user_meta(
            $user_id,
            "niubizpayDate",
            strtotime($fecha_actual."+ 1 month")
        );

        $order->payment_complete();
        $order->reduce_order_stock();
        // $order->update_status("Completed", 'Imported order', TRUE);  
        $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
        return "pago exitoso";
    }
    NIUBIZ_addnoPayRecurrentSusciption($user_id,$suscription);
    return "pago error";
}