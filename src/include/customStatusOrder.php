<?php

function NIUBIZ_register_change_suscription_order_status() {
    register_post_status( 'wc-change-suscription', array(
        'label'                     => 'Change Suscription',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Change Suscription (%s)', 'Change Suscription (%s)' )
    ) );
}
add_action( 'init', 'NIUBIZ_register_change_suscription_order_status' );


function NIUBIZ_add_change_suscription_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-change-suscription'] = 'Change Suscription';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'NIUBIZ_add_change_suscription_to_order_statuses' );
