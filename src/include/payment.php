<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'niubiz_add_gateway_class' );
function niubiz_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Niubiz_Gateway'; // your class name is here
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'niubiz_init_gateway_class' );
function niubiz_init_gateway_class() {

	class WC_Niubiz_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'niubiz'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'Niubiz Gateway';
            $this->method_description = 'Payment for Wordpress use Niubiz'; // will be displayed on the options page
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

 		}
        public function api($type = "recurrent")
        {
            $config = array(
                "MODE" => $this->testmode ? "DEV" : "PRO",
                "TYPE" => $type,
                "merchantId" => $this->get_option( 'merchantId' ),
                "merchantIdRecurrent" => $this->get_option( 'merchantIdRecurrent' ),
                "userName" => $this->get_option( 'userName' ),
                "password" => $this->get_option( 'password' ),
            );
            return new apiNiubiz($config);
        }
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Niubiz Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Niubiz',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via Niubiz',
                ),
                'currency' => array(
                    'title'       => 'Currency',
                    'type'        => 'select',
                    'default'     => 'PEN',
                    "options"     => array(
                        "PEN" => "PEN",
                        "USD" => "USD",
                    )
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
                ),
                'merchantId' => array(
                    'title'       => 'MerchantId',
                    'type'        => 'text',
                ),
                'merchantIdRecurrent' => array(
                    'title'       => 'MerchantId Recurrent',
                    'type'        => 'text',
                ),
                'userName' => array(
                    'title'       => 'User',
                    'type'        => 'text'
                ),
                'password' => array(
                    'title'       => 'Password',
                    'type'        => 'password'
                ),
                'urlPayment' => array(
                    'title'       => 'Url Payment',
                    'type'        => 'text',
                    'description' => 'Use shortcode [niubiz_payment] for show payment.',
                ),
                'imgLogoUrl' => array(
                    'title'       => 'Img Logo Url',
                    'type'        => 'text',
                    'description' => 'El tamaño sugerido es 187x40px.',
                ),
                'colorPayment' => array(
                    'title'       => 'Color Payment',
                    'type'        => 'color'
                ),
                'textButtomPament' => array(
                    'title'       => 'Text Buttom Payment',
                    'type'        => 'text'
                ),
                'urlApiSmarfit' => array(
                    'title'       => 'Url Api Smarfit',
                    'type'        => 'text'
                ),
                'authSmarfit' => array(
                    'title'       => 'Auth Smarfit',
                    'type'        => 'text'
                ),
                'authTokenSmarfit' => array(
                    'title'       => 'Auth Token Smarfit',
                    'type'        => 'text'
                ),
                'couponBySmarfit' => array(
                    'title'       => 'Coupon By User Smarfit',
                    'type'        => 'text'
                ),
            );
	 	}

		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
                // ok, let's display some description before the payment form
                if ( $this->description ) {
                    // you can instructions for test mode, I mean test card numbers etc.
                    if ( $this->testmode ) {
                        $this->description = ' TEST MODE ENABLED.';
                    }
                    // display the description with <p> tags etc.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
		}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {
            if(!$this->testmode){
                if( empty( $this->get_option( 'merchantId' ) )) {
                    wc_add_notice(  'Niubiz merchantId is required!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'userName' ) )) {
                    wc_add_notice(  'Niubiz user name is required!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'password' ) )) {
                    wc_add_notice(  'Niubiz password is required!', 'error' );
                    return false;
                }
            }
            if( empty( $this->get_option( 'urlPayment' ) )) {
                wc_add_notice(  'Niubiz urlPayment is required!', 'error' );
                return false;
            }
            //validate billing
            if( empty( $_POST[ 'billing_first_name' ]) ) {
                wc_add_notice(  'First name is required!', 'error' );
                return false;
            }
            // if( empty( $_POST[ 'billing_last_name' ]) ) {
            //     wc_add_notice(  'Last name is required!', 'error' );
            //     return false;
            // }
            if( empty( $_POST[ 'billing_email' ]) ) {
                wc_add_notice(  'Email is required!', 'error' );
                return false;
            }
            
            if ($_POST['billing_factura'] == "1") {
                if (strlen($_POST['billing_ruc']) !=11) {
                    wc_add_notice("RUC debe terner 11 caracteres", 'error');
                    return false;
                }
            }
            if($_POST['billing_type_ci'] == "0"){
                // if (strlen($_POST['billing_cedula']) !=12) {
                //     wc_add_notice("CARNET DE EXTRANJERIA debe terner 12 caracteres", 'error');
                //     return false;
                // }

            }else if($_POST['billing_type_ci'] == "1"){
                if (strlen($_POST['billing_cedula']) !=8) {
                    wc_add_notice("DNI debe terner 8 caracteres", 'error');
                    return false;
                }

            }
            return true;
		}

		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            global $woocommerce;

            return array(
                'result' => 'success',
                'redirect' => $this->get_option( 'urlPayment' ) . "?order_id=".$order_id."&".http_build_query($_POST)
            );	
	 	}
 	}
}