<?php

function Niubiz_payUser($user_id,$order_id,$token=null,$channel="web")
{
    if($token == null){
        $token = get_user_meta(
            $user_id,
            "niubiztoken",
            true
        );
    }
  
    $order = wc_get_order( $order_id );
    $niubizPayment = new WC_Niubiz_Gateway();
    $api = $niubizPayment->api();
    
    $result = $api->authorization(array(
        "channel" => $channel,
        "order" => array(
            "purchaseNumber"=> $order_id,
            "amount"=> wc_format_decimal($order->get_total(),2),
            "currency"=> $niubizPayment->get_option( 'currency' ),
        ),
        "card" => array(
            "tokenId" =>  $token,
            "registerFrequent"=> true,
            "useFrequent"=> false
        ),
        "cardHolder"=> array(
            "email"=>$order->get_billing_email()
        )
    ));
    return $result;	
}