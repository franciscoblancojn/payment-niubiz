<?php


function Niubiz_sendEmail($subject,$message,$email){
    add_filter('wp_mail_content_type', function( $content_type ) {
        return 'text/html';
    });
    wp_mail($email, $subject, $message);
}
function Niubiz_sendEmail_CancelarPlan($user)
{
    $subject = "Cancelación de Plan Smart Fit Nutri";
    $email = $user->user_email;
    $user_name = get_user_meta($user->ID,"billing_first_name",true);
    ob_start();
    require_once NIUBIZ_PATH . "src/email/template/cancelarPlan.php";
    $message = ob_get_clean();
    Niubiz_sendEmail($subject,$message,$email);
}
function Niubiz_sendEmail_CambiarPlan($user,$plan)
{
    $subject = "Cambio de Plan";
    $email = $user->user_email;
    $user_name = $user->user_login;
    $payDate = get_user_meta(
        $user->ID,
        "niubizpayDate",
        true
    );
    $dateCobro = date("d-m-Y",$payDate);
    ob_start();

    if($plan == "Plan Smart Nutri Pro"){
        $subject = "Upgrade Plan";
        require_once NIUBIZ_PATH . "src/email/template/upGrade.php";
    }else{
        $subject = "Donwgrade Plan";
        require_once NIUBIZ_PATH . "src/email/template/donwGrade.php";
    }
    $message = ob_get_clean();
    Niubiz_sendEmail($subject,$message,$email);
}
function Niubiz_sendEmail_CreateUser($user)
{
    $subject = "Bienvenido a Smart Fit Nutri";
    $comb = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); 
    $combLen = strlen($comb) - 1; 
    for ($i = 0; $i < 24; $i++) {
        $n = rand(0, $combLen);
        $pass[] = $comb[$n];
    }
    $newPassword = implode($pass);
    wp_set_password($newPassword,$user->data->ID);
    $email = array(
        "user_login"=>$user->data->user_login,
        "user_email"=>$user->data->user_email,
        "user_pass"=>$newPassword,
    );
    $email = json_decode(json_encode($email));
    ob_start();
    require_once NIUBIZ_PATH . "src/email/template/customer-new-account.php";
    $message = ob_get_clean();
    Niubiz_sendEmail($subject,$message,$user->data->user_email);
}

function Niubiz_sendEmail_Pedido($user,$plan = null)
{
    $subject = "Nuevo Pedido";
    $email = $user->data->user_email;
    $user_name = $user->data->user_login;
    ob_start();

    if($plan == null){
        $subject = "Nuevo Pedido";
        require_once NIUBIZ_PATH . "src/email/template/pedido.php";
    }else{
        $subject = "Compra de Plan";
        require_once NIUBIZ_PATH . "src/email/template/pedidoPlan.php";
    }
    $message = ob_get_clean();
    Niubiz_sendEmail($subject,$message,$email);
}