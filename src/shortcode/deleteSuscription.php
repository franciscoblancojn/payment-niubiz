<?php

function niubiz_delete_suscription() {
    ob_start();
    if(is_user_logged_in()){
        if($_POST["yesDeleteSusciprtion"] == "SI"){
            $user = wp_get_current_user();
            $user_id = $user->ID;
            delete_user_meta($user_id,"niubiztoken");
            $plan = get_user_meta($user_id,"niubizsuscription",true);
            delete_user_meta($user_id,"niubizsuscription");
            // delete_user_meta($user_id,"niubizpayDate");
            NIUBIZ_adddeleteSusciption($user_id,$plan);
            ?>
                <h1 style="margin-bottom:20px">
                    Tu plan ha sido cancelado. Si decides regresar, te estaremos esperando.
                </h1>
                <div style="text-align:center;">
                    <img src="http://checkoutpe.smartfit.com.pe/wp-content/uploads/2021/11/logo.png" alt="" style="margin:auto">
                </div>
            <?php
            Niubiz_sendEmail_CancelarPlan($user);
        }else{
            ?>
                <form method="post" class="formDeleteSusciprtion">
                    <h1>
                        ¿Esta seguro que desea eliminar la suscripción?
                    </h1>
                    <div>
                        <input value="SI" name="yesDeleteSusciprtion" type="submit" class="button" style="height: auto;" />
                    </div>
                </form>
            <?php
        }
    }else{
        ?>
            <h1 class="titleVerify">
                Por tu seguridad debemos validar tu usuario y contraseña para verificar tu identidad.
            </h1>
        <?php
        echo do_shortcode("[woocommerce_my_account]");
    }
    return ob_get_clean();
}
add_shortcode('niubiz_delete_suscription', 'niubiz_delete_suscription');