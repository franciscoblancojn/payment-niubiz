<?php

function niubiz_send_smartfitnutri($document)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://smartfitnutri.com.pe/api/v1.0/appointments/standalone/create',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'document' => $document,
            'authtoken' => get_option( 'NIUBIZ_Token' ),
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
}

function niubiz_payment() {
    ob_start();

    //validate user suscrption
    $email = $_GET["billing_email"];
    $user = get_user_by_email($email);
    if($user){
        $user_id = $user->data->ID;
        $tokenActual = get_user_meta(
            $user_id,
            "niubiztoken",
            true
        );
        if(!($tokenActual === false || $tokenActual == null || $tokenActual == "")){
            ?>
            <script>
                window.location = "/cambiar-suscipcion/"
            </script>
            <?php
            exit;
        }
    }

    $order_id = $_GET["order_id"];
    if( empty( $order_id ) ) {
        echo "Order is required!";
        return false;
    }
    $niubizPayment = new WC_Niubiz_Gateway();
    $api = $niubizPayment->api();

    $order = wc_get_order( $order_id );

    update_post_meta($order_id,"dni",$_GET["billing_cedula"]);

    if(!$order){
        echo "Order Invalid";
        return false;
    }
    if($_GET["pay"] == "true"){

        global $woocommerce;

        $transactionToken = $_POST["transactionToken"];
        $user_id = niubiz_createUser($order->get_billing_email(),$order->get_billing_first_name());
        // $user_id = $order->get_user_id();

        $result = $api->generateTokenByPay($transactionToken);

        if(200 == $result["status"]){
            $token = $result["data"]["token"]["tokenId"];
            $isRecurrent=false;

            foreach ( $order->get_items() as $item_id => $item ) {
                $product_id = $item->get_product_id();
                $typeProductNiubiz = get_post_meta($product_id,"typeProductNiubiz",true);
                if($typeProductNiubiz == "recurrent"){
                    $isRecurrent=true;
                    break;
                }
            }
            if($isRecurrent){
                $tokenActual = get_user_meta(
                    $user_id,
                    "niubiztoken",
                    true
                );
                if($tokenActual === false || $tokenActual == null || $tokenActual == ""){
                    update_user_meta(
                        $user_id,
                        "niubiztoken",
                        $token
                    );
                }
                //pago gratis 
                $pagoGratis = get_user_meta(
                    $user_id,
                    "pagogratis",
                    true
                );
                if($pagoGratis === false || $pagoGratis == null || $pagoGratis == ""){
                    $billing_cedula = get_user_meta($user_id,"billing_cedula",true);
                    $r = MISP_isSmargit(
                        $billing_cedula,
                        $niubizPayment->get_option( 'authSmarfit' ),
                        $niubizPayment->get_option( 'authTokenSmarfit' ),
                        $niubizPayment->get_option( 'urlApiSmarfit' )
                    ); 
                    if($r["type"] == "ok"){
                        $diasGratis = get_post_meta(
                            $product_id,
                            "niubiztransactionDiasGratisCliente",
                            true
                        );
                        if($diasGratis === false || $diasGratis == null || $diasGratis == ""){
                            $diasGratis = 0;
                        }
                    }else{
                        $diasGratis = get_post_meta(
                            $product_id,
                            "niubiztransactionDiasGratis",
                            true
                        );
                        if($diasGratis === false || $diasGratis == null || $diasGratis == ""){
                            $diasGratis = 0;
                        }
                    }
                    update_user_meta(
                        $user_id,
                        "pagogratis",
                        "pagogratis"
                    );
                    if($diasGratis != 0){
                        update_user_meta(
                            $user_id,
                            "niubizsuscription",
                            $product_id
                        );
                        update_user_meta(
                            $user_id,
                            "niubizsuscriptionTYPE",
                            "gratis"
                        );
                        $fecha_actual = date("d-m-Y");
                        update_user_meta(
                            $user_id,
                            "niubizpayDate",
                            strtotime($fecha_actual."+ ".$diasGratis." days")
                        );
                        // $order->payment_complete();
                        $order->update_status( 'completed' );
                        $order->reduce_order_stock();
                        $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                        $woocommerce->cart->empty_cart();
                        ?>
                            <script>
                                window.location = "<?=$order->get_checkout_order_received_url()?>"
                            </script>
                        <?php
                        return;
                    }
                }
            }else{
                $document = get_user_meta($user_id,"billing_cedula",true);
                niubiz_send_smartfitnutri($document);
            }

            $result = Niubiz_payUser($user_id,$order_id,$token);
            if(200 == $result["status"]){
                update_post_meta(
                    $order_id,
                    "niubiztransaction",
                    json_encode($result["data"])
                );
                if($isRecurrent ){
                    update_user_meta(
                        $user_id,
                        "niubizsuscription",
                        $product_id
                    );
                    $fecha_actual = date("d-m-Y");
                    update_user_meta(
                        $user_id,
                        "niubizpayDate",
                        strtotime($fecha_actual."+ 1 month")
                    );
                }

                $order->payment_complete();
                $order->reduce_order_stock();
                $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                $woocommerce->cart->empty_cart();
                ?>
                    <script>
                        window.location = "<?=$order->get_checkout_order_received_url()?>"
                    </script>
                <?php
                return;
            }
        }

        ?>
        <div class="contentError">
            <h1>
                Uppss! Ocurrio un error
            </h1>
            <p>
                Compra denegada
                
            </p>
            <p>
                #Order : <strong><?=$order_id?></strong>
            </p>
            <p>
                Fecha : <strong><?=date("d-m-Y")?></strong>
            </p>
            <br>
            <a href="http://www.smartfitnutri.com.pe/" class="btnReturnSmartfit">
				Volver a Smart fit Nutri
			</a>
        </div>
        <style>
            .contentError{
                width:500px;
                max-width:100%;
                margin:auto;
                padding: 15px;
                background:#c53c2c;
                color:#fff;
                text-align:center;
            }
            .contentError h1{
                font-size:30px;
                color:#fff;
                border-bottom:1px solid white;
            }
            .contentError p{
                font-size:20px;
                color:#fff;
            }
            .btnReturnSmartfit{
                font-size: 24px;
                display: inline-block;
                padding: 10px 25px;
                margin-bottom: 20px;
                border: 2px solid #555555;
                border-radius: 40px;
                text-transform: uppercase;
                background:#fff;
                color:#555555;
            }
        </style>
        <?php

        return ob_get_clean();
    }

    $user_id = niubiz_createUser($order->get_billing_email(),$order->get_billing_first_name());

    foreach ($_GET as $key => $value) {
        update_user_meta(
            $user_id,
            $key,
            $value
        );
    }
    $config = array(
        'action' => $niubizPayment->get_option( 'urlPayment' ) . "?pay=true&order_id=".$order_id,
        "amount" => wc_format_decimal($order->get_total(),2),
        "order_id"=>$order_id,
        "user_id"=>$user_id,
        "cardholdername" => $order->get_billing_first_name(),
        "cardholderlastname" => $order->get_billing_last_name(),
        "cardholderemail" => $order->get_billing_email(),
        "usertoken" => $order->get_user_id(),
        "timeouturl" => $niubizPayment->get_option( 'urlPayment' ) . "?order_id=".$order_id,
        "merchantlogo" => $niubizPayment->get_option( 'imgLogoUrl' ) ,
        "formbuttoncolor" => $niubizPayment->get_option( 'colorPayment' ),
        "formbuttontext" => $niubizPayment->get_option( 'textButtomPament' ),
    );

    $api->printForm($config);
    return ob_get_clean();
}
add_shortcode('niubiz_payment', 'niubiz_payment');