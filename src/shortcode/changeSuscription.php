<?php

function niubiz_change_suscription() {
    ob_start();
    if(is_user_logged_in()){
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $tokenActual = get_user_meta(
            $user_id,
            "niubiztoken",
            true
        );
        $product_id_suscription = get_user_meta(
            $user_id,
            "niubizsuscription",
            true
        );
        ?>
        <div class="contentInfoniubiz_change_suscription">
            <?php
            if($tokenActual === false || $tokenActual == null || $tokenActual == ""){
                ?>
                    <h1>
                        No tienes Suscripción
                    </h1>
                <?php
            }else{
                if($_GET["changeSusciprtion"] == "true"){
                    $order_id = $_GET["order_id"];
                    $niubizPayment = new WC_Niubiz_Gateway();
                    $api = $niubizPayment->api();
                    $transactionToken = $_POST["transactionToken"];
                    $result = $api->generateTokenByPay($transactionToken);

                    if(200 == $result["status"]){
                        $token = $result["data"]["token"]["tokenId"];
                        update_user_meta(
                            $user_id,
                            "niubiztoken",
                            $token
                        );
                        $oldPlan = get_user_meta($user_id,"niubizsuscription",true);
                        $newPlan = $_GET["product_id"];
                        update_user_meta(
                            $user_id,
                            "niubizsuscription",
                            $_GET["product_id"]
                        );
                        $order = new WC_Order($order_id);
                        $order->update_status('change-suscription');
                        NIUBIZ_addchangePlan($user_id,$oldPlan,$newPlan);
                        ?>
                            <h1>
                                Actualización exitosa
                            </h1>
                            <br>
                            <div style="text-align:center;width:100%;">
                                <a href="http://www.smartfitnutri.com.pe/login" class="btnReturnSmartfit">
                                    Volver a Smart fit Nutri
                                </a>
                            </div>
                            <style>
                            .btnReturnSmartfit{
                                font-size: 24px;
                                display: inline-block;
                                padding: 10px 25px;
                                margin-bottom: 20px;
                                border: 2px solid #555555;
                                border-radius: 40px;
                                text-transform: uppercase;
                                background: #fbba00;
                            }
                            </style>
                        <?php
                        $product = wc_get_product( $_GET["product_id"] );
                        Niubiz_sendEmail_CambiarPlan($user,$product->get_name());
                    }else{
                        ?>
                        <div class="contentError">
                            <h1>
                                Uppss! Ocurrio un error
                            </h1>
                            <p>
                                Cambio denegado
                                
                            </p>
                            <p>
                                #Order : <strong><?=$order_id?></strong>
                            </p>
                            <p>
                                Fecha : <strong><?=date("d-m-Y")?></strong>
                            </p>
                            <br>
                        </div>
                        <style>
                            .contentError{
                                width:500px;
                                max-width:100%;
                                margin:auto;
                                padding: 15px;
                                background:#c53c2c;
                                color:#fff;
                                text-align:center;
                            }
                            .contentError h1{
                                font-size:30px;
                                color:#fff;
                                border-bottom:1px solid white;
                            }
                            .contentError p{
                                font-size:20px;
                                color:#fff;
                            }
                            .btnReturnSmartfit{
                                font-size: 24px;
                                display: inline-block;
                                padding: 10px 25px;
                                margin-bottom: 20px;
                                border: 2px solid #555555;
                                border-radius: 40px;
                                text-transform: uppercase;
                                background:#fff;
                                color:#555555;
                            }
                        </style>
                    <?php
                    }
                }else if($_GET["time"] == "out"){
                    ?>
                    <h1>
                        Upss! Se paso el tiempo
                    </h1>
                    <?php
                }else if($_POST["yesChangeSusciprtion"] == "SI"){
                    $suscription = $_POST["suscription"];
                    $niubizPayment = new WC_Niubiz_Gateway();
                    $api = $niubizPayment->api();
                    $customer = new WC_Customer( $user_id );
                    $address = array(
                        'first_name' => $customer->get_billing_first_name(),
                        'last_name'  => $customer->get_billing_last_name(),
                        'company'    => $customer->get_billing_company(),
                        'email'      => $customer->get_email(),
                        'phone'      => get_user_meta( $user_id, 'billing_phone', true ),
                        'address_1'  => $customer->get_billing_address_1(),
                        'address_2'  => $customer->get_billing_address_2(),
                        'city'       => $customer->get_billing_city(),
                        'state'      => $customer->get_billing_state(),
                        'postcode'   => $customer->get_billing_postcode(),
                        'country'    => $customer->get_billing_country()
                    );
                    $billing_cedula = get_user_meta($user_id,"billing_cedula",true);
                    $order = wc_create_order();
                    $order->add_product( get_product($suscription), 1);
                    $order->set_address( $address, 'billing' );
                    $r = MISP_isSmargit(
                        $billing_cedula,
                        $niubizPayment->get_option( 'authSmarfit' ),
                        $niubizPayment->get_option( 'authTokenSmarfit' ),
                        $niubizPayment->get_option( 'urlApiSmarfit' )
                    ); 
                    if($r["type"] == "ok"){
                        //addDiscounte
                        $coupon = explode(",",$niubizPayment->get_option( 'couponBySmarfit' ));
                        for ($i=0; $i < count($coupon); $i++) { 
                            $order->apply_coupon($coupon[$i]);
                        }
                    }   
                    $order->calculate_totals();
                    $order_id = $order->get_id();
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $config = array(
                        'action' => $actual_link."?changeSusciprtion=true&order_id=".$order_id."&product_id=".$suscription,
                        "amount" => wc_format_decimal($order->get_total(),2),
                        "order_id"=>$order_id,
                        "user_id"=>$user_id,
                        "cardholdername" => $order->get_billing_first_name(),
                        "cardholderlastname" => $order->get_billing_last_name(),
                        "cardholderemail" => $order->get_billing_email(),
                        "usertoken" => $order->get_user_id(),
                        "timeouturl" => $actual_link. "?time=out&order_id=".$order_id,
                        "merchantlogo" => $niubizPayment->get_option( 'imgLogoUrl' ) ,
                        "formbuttoncolor" => $niubizPayment->get_option( 'colorPayment' ),
                        "formbuttontext" => $niubizPayment->get_option( 'textButtomPament' ),
                    );
                    $api->printForm($config);
                    ?>
                    <style>
                        #contentSusciption > div{
                            background: transparent;
                        }
                    </style>
                    <?php
                }else{
                    $product_ids = wc_get_products(
                        array(
                            'exclude' => array( $product_id_suscription ),
                            'return' => 'ids',
                        )
                    );
                    ?>
                        <form method="post" class="formChangeSusciprtion">
                            <h1 class="titleVerify" style="margin-bottom:0">
                                Estas cambiando tu plan a 
                                <?php
                                    for ($i=0; $i < count($product_ids); $i++) { 
                                        $product_id = $product_ids[$i];
                                        $product = wc_get_product( $product_id );
                                        $typeProductNiubiz = get_post_meta($product_id,"typeProductNiubiz",true);
                                        if($typeProductNiubiz == "recurrent"){
                                            echo $product->get_name();
                                        }
                                    }
                                ?>
                            </h1>
                            <select name="suscription" id="suscription" class="woocommerce-form-row" style="display:none;">
                                <?php
                                    for ($i=0; $i < count($product_ids); $i++) { 
                                        $product_id = $product_ids[$i];
                                        $product = wc_get_product( $product_id );
                                        $typeProductNiubiz = get_post_meta($product_id,"typeProductNiubiz",true);
                                        if($typeProductNiubiz == "recurrent"){
                                            ?>
                                            <option value="<?=$product_id?>">
                                                <?=$product->get_name()?>
                                            </option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                            <h1 class="titleVerify" style="margin-bottom:0">
                                ¿Estás seguro?
                            </h1>
                            <input type="submit" class="button" name="yesChangeSusciprtion" id="yesChangeSusciprtion" value="SI" style="font-size:19px;margin-bottom:0;margin-top:0;"/>
                        </form>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }else{
        ?>
            <h1 class="titleVerify">
                Por tu seguridad debemos validar tu usuario y contraseña para verificar tu identidad.
            </h1>
        <?php
        echo do_shortcode("[woocommerce_my_account]");
    }
    return ob_get_clean();
}
add_shortcode('niubiz_change_suscription', 'niubiz_change_suscription');