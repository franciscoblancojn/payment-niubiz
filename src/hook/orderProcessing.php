<?php 
function NIUBIZ_orderProcessing($order_id) {
    $order = wc_get_order( $order_id );
    $email = $order->get_billing_email();
    $user = get_user_by_email($email);
    if($user){
        $orderArg = array(
            'customer' => $email,
            'limit' => -1,
            'status'=> array( 'wc-completed','wc-processing' ),
            );
            //add processing and complete for days free
        $orders = wc_get_orders($orderArg);
        if(count($orders) == 1){
            Niubiz_sendEmail_CreateUser($user);
        }
        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $planName = $item->get_name();
        }
    
        $typeProduct = get_post_meta($product_id,"typeProductNiubiz",true);
    
        $user = get_user_by_email($email);
        if($typeProduct == "recurrent"){
            Niubiz_sendEmail_Pedido($user,$planName);
        }else{
            Niubiz_sendEmail_Pedido($user);
        }
    }

}
add_action('woocommerce_order_status_processing',   'NIUBIZ_orderProcessing' , 10, 1);  
add_action('woocommerce_order_status_completed',   'NIUBIZ_orderProcessing' , 10, 1);  